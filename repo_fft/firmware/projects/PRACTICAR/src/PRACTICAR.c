/*
 * CÃ¡tedra: ElectrÃ³nica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 * RevisiÃ³n:
 * 07-02-18: VersiÃ³n inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/parcial1c2019.h"
#include "systemclock.h"
#include "analog_io.h"
#include "led.h"
#include "DisplayITS_E0803.h"
#include "uart.h"
#include "timer.h"
#include "gpio.h"
#include "bool.h"

/*==================[macros and definitions]=================================*/

uint16_t valor=0;
bool init=false;
uint16_t contador=100;

/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

//Adquirir por cada flanco descendente proveniente del GPIO7

void InitReadSignal (void)
{
	init=true;
}
void noInitReadSignal (void)
{
	init=false;
}

void ReadSignal (void)
{
	if (init)
	{
	AnalogInputRead(CH1,&valor);
	}
}

void interruptionAnalog(void)
{
	AnalogStartConvertion();
	contador--;
}

// comando remoto por serial

void RemoteCommand(void)
{
	char *cls = "\033[2J\033[;f"; //limpia
	uint8_t dat = 0;

	if(TRUE == UartReadByte(SERIAL_PORT_PC,&dat))
	{
		switch(dat)
		{

			case '1':
					LedOff(LED_RGB_R);
					LedOn(LED_RGB_G);
					UartSendBuffer(SERIAL_PORT_PC, cls, strlen(cls));
					TimerStart(TIMER_B);
					break;

			case '2':
					LedOff(LED_RGB_G);
					LedOn(LED_RGB_R);
					UartSendBuffer(SERIAL_PORT_PC, cls, strlen(cls)); // podria comentarse en caso de querer retener los valores mostrados
					TimerStop(TIMER_B);
					break;

			default:
					UartSendBuffer(SERIAL_PORT_PC, "Comando Incorrecto!\n\r", strlen("Comando Incorrecto!\n\r"));
					break;
		}
	}
}

int main(void)
{

	SystemClockInit();


	//InicializaciÃ³n de Conversor AD
	analog_input_config Presion;
	Presion.input=CH1;
	Presion.mode=AINPUTS_SINGLE_READ;
	Presion.pAnalogInput=&ReadSignal;

	gpio_t pins[7];
		pins[0]=LCD1;
		pins[1]=LCD2;
		pins[2]=LCD3;
		pins[3]=LCD4;
		pins[4]=GPIO1;
		pins[5]=GPIO3;
		pins[6]=GPIO5;
	ITSE0803Init(pins);
	ITSE0803DisplayValue(0);

	//LEDS, inicio en rojo
		LedsInit();
		LedOn(LED_RGB_R);
	//

	GPIOInit(GPIO7,0); //0=GPIO_INPUT
	GPIOOn(GPIO7); //activamos GPIO7

	//INTERRUPCION POR FLANCO DESCENDENTE (ULTIMO PARAMETRO FALSE) DEL GPIO7 - INICIO DE ADQUISICION
	GPIOActivInt(GPIOGP7,GPIO7,&InitReadSignal,0);
	//INTERRUPCION POR FLANCO ASCENDENTE (ULTIMO PARAMETRO TRUE) DEL GPIO7 - FIN DE ADQUISICION
	GPIOActivInt(GPIOGP7,GPIO7,&noInitReadSignal,1);

	AnalogInputInit(&Presion);
	//

	timer_config t= {TIMER_B,10,interruptionAnalog};
	TimerInit(&t);

	// TimerStart(TIMER_B);

	uint16_t comparador=0;
	uint16_t Pmax;

	/* SERIAL COMUNICATION */

	serial_config serial = {SERIAL_PORT_PC, 115200, &RemoteCommand};

	UartInit(&serial);

	while(1)
		{
	    	if (contador>0)
	    	{
	    		if (valor>comparador)
	    		{
	    			comparador=valor;
	    		}
	    	}
	    	else {contador=100;
	    	//convierto el valor a mmHG
	    	Pmax= (comparador*125)/3.3;
	    	//envio por puerto serie la PA maxima
	    	UartSendString(SERIAL_PORT_PC,UartItoa(Pmax,10));
	    	UartSendString(SERIAL_PORT_PC," mmHg \n\r");
	    	ITSE0803DisplayValue(Pmax);}
		}

	return 0;
}

/*==================[end of file]============================================*/

