﻿Teoria:

1) a) Las excepciones son eventos inesperados que ocurren mientras se ejecuta un programa. Lo cual hace que se detenga la normal ejecución, se guarden los datos 
en el registro de pila mediante el uso de punteros y luego de resolverse la excepcion, puede continuar por el lugar donde estaba.
Son generados por el procesador: lo cual puede venir de fallas o del software (sistema operativo). Las excepciones vienen definidas por el fabricante. 

Según la clasificación de ARM, las interrupciones son un tipo de excepciones generadas habitualmente por periféricos de comunicación (UART-USART),
temporizadores y puertos de I/O GPIO. Se encuentran las interrupciones de vectores anidados (dentro del chip) -->NVIC y tambien estan las interrupciones
NO enmascaradas (NMI). 
Las interrupciones a diferencia de las excepciones, pueden ser configuradas con su distinto nivel prioridad y tipo, por el programador.

b) Los numeros hexadecimales presentados a la izquierda del registro representan la dirección de memoria de cada uno de los módulos del Microprocesador, 
asi cuando ocurre una excepcion puede saber a que lugar de memoria debe volver. MSP es el puntero al vector que recorrera el stack(pila).

2) RIT TIMER: es un temporizador de intervalo repetidos que nace debido a que el Systick Timer se usa principalmente en la ejecución Sistemas Operativos en tiempo real.
Como su nombre lo indica trabaja a intervalos de tiempo fijos.
Las compuertas OR ubicadas a la salida del COMPARATOR lo que hacen es sumar los dos datos de entrada, uno proveniente del comparador y el otro del registro de
máscaras, para que si alguno está en alto, pase ese valor a la compuerta AND y luego al FF.