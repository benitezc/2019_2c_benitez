/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2019
 * Autor/es:
 * Virginia Benitez Cerquetella --> vir_be@hotmail.com
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Recuperatorio.h"       /* <= own header */

#include "systemclock.h"
#include "led.h"
#include "string.h"
#include "DisplayITS_E0803.h"
#include "analog_io.h"
#include "timer.h"
#include "switch.h"
#include "bool.h"
#include "uart.h"
#include "gpio.h"

/*==================[macros and definitions]=================================*/
/*==================[internal data definition]===============================*/
uint16_t valor=0;
uint16_t v_crudo;
uint16_t v_filtrado;
uint16_t v_anterior=0;
uint32_t lpf_beta;		//respuesta en frecuencia del filtro
/*==================[internal functions declaration]=========================*/

//Inicia la conversion analogico-digital
void IniciarConversion(){
		AnalogStartConvertion();}

//lee la señal analógica del CH1 y lo envia a la PC por puerto serie
//como valor crudo, valor filtrado (enter)
void ReadSignal (void){
	AnalogInputRead(CH1,&valor);
	v_crudo=valor;
	v_filtrado=v_anterior-lpf_beta*(v_anterior-v_crudo);
	v_anterior=v_filtrado;
	UartSendString(SERIAL_PORT_P2_CONNECTOR, UartItoa(v_crudo, 10));
	UartSendString(SERIAL_PORT_P2_CONNECTOR, ",  ");
	UartSendString(SERIAL_PORT_P2_CONNECTOR, UartItoa(v_filtrado, 10));
	UartSendString(SERIAL_PORT_P2_CONNECTOR, " \r \n");}


//Dependiendo de la tecla que se presione, sera el valor que toma beta y el valor que se muestra en el display
void Tecla2(){
	lpf_beta  = 0.25;
	ITSE0803DisplayValue(25);}

void Tecla3(){
	lpf_beta  = 0.55;
	ITSE0803DisplayValue(55);}

void Tecla4(){

	lpf_beta  = 0.75;
	ITSE0803DisplayValue(75);}


//funcion vacía para parámetro *pSerial del inicializador de la UART */
void Useless(){
	}


/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{
	SystemClockInit();
	SwitchesInit();

		//inicializacion de interrupciones
		timer_config i_timer;
		i_timer.timer=TIMER_B;
		i_timer.period=10;			//cada 10 ms hay una muestra
		i_timer.pFunc=IniciarConversion;
	TimerInit(&i_timer);
	TimerStart(TIMER_B);

		//inicialización de conversor AD
		analog_input_config senial;
		senial.input=CH1;
		senial.mode=AINPUTS_SINGLE_READ;
		senial.pAnalogInput=ReadSignal;
	AnalogInputInit(&senial);

	//Inicialización pantalla LCD
		gpio_t pins[7];
				pins[0]=LCD1;
				pins[1]=LCD2;
				pins[2]=LCD3;
				pins[3]=LCD4;
				pins[4]=GPIO1;
				pins[5]=GPIO3;
				pins[6]=GPIO5;
		ITSE0803Init(pins);

		//comunicación serial
		serial_config i_puerto;
		i_puerto.port=SERIAL_PORT_PC;
		i_puerto.baud_rate=115200;
		i_puerto.pSerial=Useless;
	UartInit(&i_puerto);

		//Depende de que tecla presiono llamo a la funcion correspondiente
			SwitchActivInt(SWITCH_2,Tecla2);
			SwitchActivInt(SWITCH_3,Tecla3);
			SwitchActivInt(SWITCH_4,Tecla4);
		while (1){
			}
	return 0;}

/*==================[end of file]============================================*/

