﻿Universidad Nacional de Entre Rios - Facutad de Ingeniería
rECUPERATORIO

Catedra: Electronica Programable - 2° cuatrimestre 2019

Autor: Benitez Cerquetella, Virginia

Bibliotecas utilizadas

systemclock.h
analog_io.h
timer.h 
gpio.h
uart.h
bool.h
led.h
string.h
DisplayITS_E0803.h
switch.h


Montaje e información importante

La salida digital de un acelerómetro debe ir al canal analógico CH1 de la EDU-CIAA
La frecuencia de muestreo de la señal es de 100 hz. 

Lo primero que hago es inicializar las interrucpciones con el timer B, dando una muestra cada 10 ms. 
Inicia la conversión analógico digital y lee la señal del CH1, pasandola por puerto serie a la PC. Mediante una funcion de filtrado, 
lo que se manda por la UART es el valor crudo leido, valor filtado.
Luego las 3 funciones Teclas, lo que hacen es detectar que tecla se presiona, darle un valor flotante específico a beta, y mostrar
en el LCD el valor en decimales de beta. 

Las funciones estan comentadas en el codigo.
Solo a modo de aclaración y despues de haber consultado: me aparece este error al compilar
make: *** No rule to make target 'all'.  Stop.

Puede que al resolverse esto, aparezcan otros que no pude resolver porque no me los mostro...