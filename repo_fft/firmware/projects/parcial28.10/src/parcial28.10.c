/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "parcial.h"
#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "analog_io.h"
#include "timer.h"
#include "bool.h"
#include "gpio.h"
#include "string.h"
#include "DisplayITS_E0803.h"
#include "uart.h"



/*==================[macros and definitions]=================================*/
#define MMHG_MAX 200
#define MMHG_MIN 0
#define MMHG_ALERT_MAX 150
#define MMHG_ALERT_MID 50
#define VOLT_MAX 3.3


uint16_t valor;
uint16_t sumador=0;
uint16_t contador = 250; //numero de muestras en 1 segundo.
uint16_t pa;
uint16_t pa_max=0;
uint16_t pa_min=500;
uint16_t pa_promedio;
bool opcion1=true; //La app arranca mostrando valor máximo
bool opcion2=false; //Para opción de mostrar mínimo
bool opcion3=false; //Para opción de mostrar promedio

/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/


/*
 * Función que lee el valor desde el ADC y relaiza la lógica
 * pedida: calcular presiones max, min y promedio en 1 segundo
 * y mostrarlas cada un segundo en LCD y mandarlas a PC
 */
void ReadSignal (){
	 AnalogInputRead(CH1,&valor);

	 pa=valor*MMHG_MAX/VOLT_MAX; //Transformo el valor en voltaje a valores de presión

	 if(contador>0) //durante 250 muestras voy sumando las muestras, y encontrando le maximo y minimo
	 {
		sumador=sumador+pa;	//sumo todos los valores de presion
		if(pa>pa_max)
			pa_max=pa; 		//encuentro el max
		if(pa_min>pa)
			pa_min=pa;		//encuentro presion minima
		contador--;
	 }
	 else
	 {
		contador=250;				//pasada la toma de 250 muestras (1 segundo) realizo lo solicitado
		pa_promedio=sumador/250;
		if((pa_max>MMHG_ALERT_MID)&&(pa<MMHG_ALERT_MAX))
		{
			LedOn(LED_RGB_R);
		}
		else
		{
			if(pa_max<MMHG_ALERT_MAX)
				LedOn(LED_RGB_G);
			else
				LedOn(LED_RGB_B);
		}

		//Cada 1 segundo muestro los valores por el LCD y los envio a la PC.
		if(opcion1==true)
		{
			ITSE0803DisplayValue(pa_max);
			UartSendString(SERIAL_PORT_PC, UartItoa(pa_max, 10) );
			UartSendString(SERIAL_PORT_PC, " mmHG -> Presión Máxima \r\n");
		}
		if(opcion2==true)
		{
			ITSE0803DisplayValue(pa_min);
			UartSendString(SERIAL_PORT_PC, UartItoa(pa_min, 10) );
			UartSendString(SERIAL_PORT_PC, " mmHG -> Presion Minima \r\n");
		}
		if(opcion3==true)
		{
			ITSE0803DisplayValue(pa_promedio);
			UartSendString(SERIAL_PORT_PC, UartItoa(pa_promedio, 10) );
			UartSendString(SERIAL_PORT_PC, " mmHG -> Presion promedio \r\n");
		}
	 }
}


void AtencionInterrupcion ()//comienza la conversión
{
	AnalogStartConvertion();
}


void Tecla1()
{
	opcion1=true;
	opcion2=false;
	opcion3=false;
}

void Tecla2()
{
	opcion1=false;
	opcion2=true;
	opcion3=false;
}

void Tecla3()
{
	opcion1=false;
	opcion2=false;
	opcion3=true;
}

void Pc_Command() //Función vacia de uart
{
}

int main(void)
{

	SystemClockInit();
	LedsInit();
	SwitchesInit();

	//Inicialización pantalla LCD
	gpio_t pins[7];
			pins[0]=LCD1;
			pins[1]=LCD2;
			pins[2]=LCD3;
			pins[3]=LCD4;
			pins[4]=GPIO1;
			pins[5]=GPIO3;
			pins[6]=GPIO5;
	ITSE0803Init(pins);

	//Inicialización de Conversor AD
	analog_input_config signal;
	signal.input=CH1;
	signal.mode=AINPUTS_SINGLE_READ;
	signal.pAnalogInput=ReadSignal;

	AnalogInputInit(&signal);

	//Inicialización de Interrupciones
	timer_config temp;
	temp.timer=TIMER_B;
	temp.period=4;
	temp.pFunc=AtencionInterrupcion;//Cada cierto tiempo arranca la conversión. Termina la conversión
										//y arranca la función ReadSignal donde se lee el valor.
	TimerInit(&temp);
	TimerStart(TIMER_B);

	//Iniciaclización Comunicación Serie
	serial_config puerto;
	puerto.port = SERIAL_PORT_PC ;
	puerto.baud_rate=112600;
	puerto.pSerial=Pc_Command;
	UartInit(&puerto);

	//Inicialización teclas
	SwitchActivInt(SWITCH_1,Tecla1);
	SwitchActivInt(SWITCH_2,Tecla2);
	SwitchActivInt(SWITCH_3,Tecla3);



    while(1)
{
   if(opcion1==true)
   {
	   LedOn(LED_1);
	   LedOff(LED_2);
	   LedOff(LED_3);

   }
   if(opcion2==true)
     {
	   LedOn(LED_2);
	   LedOff(LED_1);
	   LedOff(LED_3);
     }
   if(opcion3==true)
     {
	   LedOn(LED_3);
	   LedOff(LED_1);
	   LedOff(LED_2);
     }

} //fin while

    

	return 0;
}

/*==================[end of file]============================================*/

