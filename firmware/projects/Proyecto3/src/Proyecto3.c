/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * BENITEZ CERQUETELLA, VIRGINIA - vir_be@hotmail.com
 * CHACÓN, ALEJO - alejochacon96@hotmail.com
 * ROMERO RAVANO, MARIA VICTORIA - victoriaremo@gmail.com
 * RATTI, TOMÁS - tomasratti@gmail.com
 *

 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../../Proyecto3/inc/Proyecto3.h"       /* <= own header */

#include "systemclock.h"
#include "led.h"
#include "bool.h"
#include "timer.h"
#include "switch.h"
#include "hc_sr4.h"
#include "uart.h"
#include "gpio.h"

/*==================[macros and definitions]=================================*/
bool Unidad= true;
int16_t Distancia=0;
/*==================[internal data definition]===============================*/
/*==================[internal functions declaration]=========================*/



/**@brief Funcion para obtener la distancia del sensor ultrasonico HC-SR04
 * y lo envia por protocolo RS232 al módulo HC-06 (bluetooth)
 * @param[in] ninguno
 * @return (void)
 */

void ObtenerDistancia(){

	if(Unidad==true){
		Distancia=HcSr04ReadDistanceCentimeters();
	}
	else {
		Distancia=HcSr04ReadDistanceInches();
	}

	UartSendString(SERIAL_PORT_P2_CONNECTOR, UartItoa(Distancia, 10));

	if(Unidad==true){
		UartSendByte (SERIAL_PORT_P2_CONNECTOR, " cm \r \n");
	}
	else{
		UartSendByte (SERIAL_PORT_P2_CONNECTOR, " in \r \n");
	}
}

/**@brief Funcion para cambiar la unidad de medición
 * enciende un led si la medición esta en pulgadas=in
 * @param[in] ninguno
 * @return (void)
 */

void CambiarUnidad(){
	if (Unidad==true){
		Unidad=false;
		LedOn(LED_RGB_B);
	}
	else {
		Unidad=true;
		LedOff(LED_RGB_B);
	}
}

/* funcion vacía para parámetro *pSerial del inicializador de la UART */
 void Vacia(){
}

/*==================[external data definition]===============================*/
/*==================[external functions definition]==========================*/

int main(void)
{
	SystemClockInit();
	HcSr04Init(T_FIL2, T_FIL3);
	LedsInit();

	timer_config Timer_inic;

	Timer_inic.timer=TIMER_B;
	Timer_inic.period=1000;
	Timer_inic.pFunc=ObtenerDistancia();
	TimerInit(&Timer_inic);

	serial_config UART_inic;

	UART_inic.port=SERIAL_PORT_P2_CONNECTOR;
	UART_inic.baud_rate=9600;
	UART_inic.pSerial=Vacia;
	UartInit(&UART_inic);

	SwitchesInit();
	SwitchActivInt(SWITCH_1, CambiarUnidad());
	TimerStart(TIMER_B);
	while (1){
		}
	}


/*==================[end of file]============================================*/
