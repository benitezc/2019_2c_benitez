/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * BENITEZ CERQUETELLA, VIRGINIA - vir_be@hotmail.com
 * CHACÓN, ALEJO - alejochacon96@hotmail.com
 * ROMERO RAVANO, MARIA VICTORIA - victoriaremo@gmail.com
 * RATTI, TOMÁS - tomasratti@gmail.com
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/SensorIF.h"       /* <= own header */

#include "DisplayITS_E0803.h"
#include "systemclock.h"
#include "led.h"
#include "bool.h"
#include "delay.h"
#include "switch.h"
#include "Tcrt5000.h"
#include <stdint.h>
#include "gpio.h"

/*==================[macros and definitions]=================================*/
/*==================[internal data definition]===============================*/
uint8_t LineCount=0;
bool Prendido=true;
bool Contando=true;
/*==================[internal functions declaration]=========================*/

void Tecla_1 (void){
		if(Prendido==true){
			Prendido=false;
			LedOff(LED_1);
			LedOff(LED_2);
			LedOff(LED_3);
}
		else {
			Prendido=true;
			LedOn(LED_3);}
	}
//
void Tecla_2 (void){
		if (Contando==true){
			LedOn(LED3);
			LedOff(LED2);
			LedOff(LED1);
			Contando=false;}
		else{
			LedOff(LED3);
			LedOn(LED2);
			Contando=true;}
}
//contador se pone a cero
void Tecla_3 (void){
	LedOn(LED1);
	LineCount=0;}

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

/** Aplicación C para utilizar sensor infrarrojo Tcrt5000 como contador de obstáculos
 * El valor obtenido se presenta en display ITS_E0803
 * Led rojo de EDU-CIAA indica
 * La cuenta se incrementa cada vez que el sensor detecta un obstaculo (led verde se apaga, led rojo se enciende)
 * Presionando tecla 1 de EDU-CIAA se reinicia la cuenta
 */

int main(void)
{
		gpio_t x;
	    x= T_COL0;
	    bool PreviousState, ActualState;

		SystemClockInit();
		Tcrt5000Init(x);
		LedsInit();
		PreviousState=ActualState=Tcrt5000State();

		gpio_t pins[7];
			pins[0]=LCD1;
			pins[1]=LCD2;
			pins[2]=LCD3;
			pins[3]=LCD4;
			pins[4]=GPIO1;
			pins[5]=GPIO3;
			pins[6]=GPIO5;

		ITSE0803Init(pins);
		SwitchesInit();
		SwitchActivInt(SWITCH_1, Tecla_1);
		SwitchActivInt(SWITCH_2, Tecla_2);
		SwitchActivInt(SWITCH_3, Tecla_3);

		while (1){
	if(Contando==true){
	ActualState=Tcrt5000State();
	if (ActualState==0){
		if(ActualState!=PreviousState){
			LineCount++;
	}
	}
	}
	PreviousState=ActualState;

	if (Prendido==true){
			ITSE0803DisplayValue(LineCount);
			}
	else{
			ITSE0803ValueOff();
			}
		}


    return 0;}

/*==================[end of file]============================================*/
