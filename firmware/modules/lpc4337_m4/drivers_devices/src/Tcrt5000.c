/*
 * Tcrt5000.c
 *
 *  Created on: 6 sep. 2019
 *      Author:
 * BENITEZ CERQUETELLA, VIRGINIA - vir_be@hotmail.com
 * CHACÓN, ALEJO - alejochacon96@hotmail.com
 * ROMERO RAVANO, MARIA VICTORIA - victoriaremo@gmail.com
 * RATTI, TOMÁS - tomasratti@gmail.com
 */

#include "Tcrt5000.h"
#include <stdint.h>


gpio_t MiPin;


bool Tcrt5000Init(gpio_t dout){
	/* Inicializacion del pin dout*/
	 GPIOInit(dout, GPIO_INPUT);
	 MiPin=dout;
	 bool Resultado;
	 if (LEDRGB_B<dout){
		 Resultado=true;}
	 else {
		 Resultado=false;}

	 return (Resultado);
 }

bool Tcrt5000State(){
	/* Indica el estado del pin*/
	bool estado;
	estado= GPIORead(MiPin);
	return estado;
}

bool Tcrt5000Deinit(){
	GPIODeinit();
}

