/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>

/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/

/*B) Defina un vector de estructuras llamado menuPrincipal.
 * Cada estructura cuenta con un campo txt del tipo char que contiene la etiqueta de la opción de menú,
 * y un campo doit qué es un puntero a una función a ejecutar al seleccionar este ítem del menú

typedef struct {
    char * txt ;      	 Etiqueta de la opción
    void (*doit)() ;     Función a ejecutar en caso de seleccionar esa opción
} menuEntry ;

menuEntry menuPrincipal [] =
{
   	 { "etiqueta" , punt_a_función},
…….
};

Escriba una función que reciba como parámetro un puntero al vector la opción de menú a ejecutar y, empleando punteros a función ejecute.

EjecutarMenu(menuEntry * menu , int option)
{}*/

typedef struct{
	    char  *txt ;      	/* Etiqueta de la opción */
	    void (*doit)() ;    /* Función a ejecutar en caso de seleccionar esa opción */
	} menu ;


	void Led_verde (){
		printf ("Se enciende el led verde");
	};

	void Led_rojo (){
		printf ("Se enciende el led rojo");
	};

	void Led_azul (){
		printf ("Se enciende el led azul");
	};

	void EjecutarFuncion (menu *opcion, int seleccion){
		opcion[seleccion-1].doit();
	};

int main(void)
{
	menu menuPrincipal [] = {
			{"1: Prende_Led_Verde", Led_verde},
			{"2: Prende_Led_Rojo", Led_rojo},
			{"3: Prende_Led_Azul", Led_azul},
		};
EjecutarFuncion(menuPrincipal, 2);
	return 0;
}

/*==================[end of file]============================================*/

