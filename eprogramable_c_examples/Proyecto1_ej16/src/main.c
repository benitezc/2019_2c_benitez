/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include "stdio.h"

/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/
//a.	Declare una variable sin signo de 32 bits y cargue el valor 0x01020304.
//Declare cuatro variables sin signo de 8 bits y, utilizando máscaras, rotaciones y truncamiento, cargue cada uno de los bytes de la variable de 32 bits.
//b.	Realice el mismo ejercicio, utilizando la definición de una “union”.


int main(void)
{ 	uint32_t A;
	A=0x01020304;
	uint8_t B;
	uint8_t C;
	uint8_t D;
	uint8_t E;

	B=A;
	C=A>>8;
	D=A>>16;
	E=A>>24;

printf("El valor de A es %d \n", A);
printf("El valor de B es %d \n", B);
printf("El valor de C es %d \n", C);
printf("El valor de D es %d \n", D);
printf("El valor de E es %d \n", E);

union test{
    struct cada_byte{
        uint8_t byte1;
        uint8_t byte2;
        uint8_t byte3;
        uint8_t byte4;
    } var_s;
    uint32_t  todoslosbytes;
} var_u;

var_u.todoslosbytes=0x01020304;

printf("El valor de byte1 es %d \n", var_u.var_s.byte1);
printf("El valor de byte2 es %d \n", var_u.var_s.byte2);
printf("El valor de byte3 es %d \n", var_u.var_s.byte3);
printf("El valor de byte4 es %d \n", var_u.var_s.byte4);
printf("El valor de TodosLosBytes es %d \n", var_u.todoslosbytes);


   // while(1)
    //{ }
	return 0;
}

/*==================[end of file]============================================*/

