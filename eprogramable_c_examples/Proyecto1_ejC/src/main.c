/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>

/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/

/*C) Escriba una función que reciba un dato de 32 bits, la cantidad de dígitos de salida
 * y un puntero a un arreglo donde se almacene los n dígitos.
 * La función deberá convertir el dato recibido a BCD, guardando cada uno de los dígitos de salida en el arreglo pasado como puntero.

BinaryToBcd (uint32_t data, uint8_t digits, uint8_t * bcd_number )
{}
 */

void Binario_a_BCD (uint32_t dato, uint8_t digitos_salida, uint8_t *bcd_num )
{	uint32_t dato_cortado;
	uint8_t i;
	for (i=0 ; i<digitos_salida ; i++){
	dato_cortado= dato%10; 				/* me guarda el resto de la division por 10 o el ultimo digito */
	bcd_num [digitos_salida-1-i]= dato_cortado; 	/* pongo en el arreglo el digito cortado */
	dato=dato/10;}						/* le saco el ultimo digito al dato */

}

int main(void)
{
	uint8_t arreglo[10]= {0,0,0,0,0,0,0,0,0,0};

	uint32_t num =123456;
	uint8_t dig=2;
	uint8_t j;
	/* LLamo a la función  */
	Binario_a_BCD (num, dig, arreglo);

	for (j=0 ; j<dig ; j++){
	printf( "Numero en BCD %d \n", arreglo[j]);

	}
	return 0;
}

/*==================[end of file]============================================*/

