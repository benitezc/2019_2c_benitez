/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>

/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/
/*A) Realice un función  que reciba un puntero a una estructura LED como la que se muestra a continuación:
struct leds{
	uint8_t n_led;      indica el número de led a controlar
	uint8_t n_ciclos;   indica la cantidad de ciclos de encendido/apagado
	uint8_t periodo;    indica el tiempo de cada ciclo
	uint8_t mode;       ON, OFF, TOGGLE
}my_leds,
 * */

int main(void){

	enum Ledmode  {ON, OFF, TOGGLE};
	uint8_t i,j;

typedef struct
{
	uint8_t n_led;      //indica el número de led a controlar
	uint8_t n_ciclos;   //indica la cantidad de cilcos de encendido/apagado
	uint8_t periodo;    //indica el tiempo de cada ciclo
	uint8_t mode;
} leds;


void AtiendeLeds(leds  my_leds){

if (my_leds.mode==ON){
	if (my_leds.n_led==1)
		printf("Se prende el LED 1");
	else if (my_leds.n_led==2)
		printf("Se prende el LED 2");
	else if (my_leds.n_led==3)
		printf("Se prende el LED 3");}

else if (my_leds.mode==OFF){
		if (my_leds.n_led==1)
		printf("Se apaga el LED 1");
		else if (my_leds.n_led==2)
		printf("Se apaga el LED 2");
		else if (my_leds.n_led==3)
		printf("Se apaga el LED 3");}

else if (my_leds.mode==TOGGLE){
		if (i<my_leds.n_ciclos){
			if (my_leds.n_led==1)
				printf("Toggle LED 1");		/*El TOGGLE se implementa como APAGA RETARDO y ENCIENDE RETARDO*/
			else if (my_leds.n_led==2)
				printf("Toggle LED 2");
			else if (my_leds.n_led==3)
				printf("Toggle LED 3");
			i++;}

		for (j=0;j<my_leds.periodo;j++);

		}
}
	return 0;
}

/*==================[end of file]============================================*/

